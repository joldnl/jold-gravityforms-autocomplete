<?php

/*
    Plugin Name:           Gravity Forms: Autocomplete Fields
    Description:           Configure autocomplete attributes on Gravity Forms' inputs.
    Version:               0.1.2
    Plugin URI:            https://bitbucket.org/joldnl/jold-gravityforms-autocomplete
    Bitbucket Plugin URI:  https://bitbucket.org/joldnl/jold-gravityforms-autocomplete
    Author:                Jurgen Oldenburg
    Author URI:            http://www.jold.nl
    License:               GPLv2 or later
    License URI:           http://www.gnu.org/licenses/gpl-2.0.html
*/

namespace Jold\GravityForms\AutocompleteFields;

define( 'GFORM_AUTOCOMPLETEFIELDS_VERSION', '0.1.2' );

require_once __DIR__ . '/includes/core.php';
